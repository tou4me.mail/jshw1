// #1
var x = 6;
var y = 14;
var z = 4;

x += y - x++ * z;
var result = x;
document.write('x += y - x++ *z =' + result + '<hr>');

// #2
var x = 6;
var y = 14;
var z = 4;

z = --x - y * 5;
var result = z;
document.write('z = --x - y * 5 =' + result + '<hr>');

//   #3
var x = 6;
var y = 14;
var z = 4;

y /= x + (5 % z);
var result = y;
document.write('y /= x + (5 % z) =' + result + '<hr>');

//   #4
var x = 6;
var y = 14;
var z = 4;

//   z - x++ + y * 5;
var result = z - x++ + y * 5;
document.write('z - x++ + y * 5 =' + result + '<hr>');

//   #5
var x = 6;
var y = 14;
var z = 4;

x = y - x++ * z;
var result = x;
document.write('x = y - x++ * z =' + result + '<hr>');
